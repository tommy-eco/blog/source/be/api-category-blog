﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Command.Create;
using Application.Command.Delete;
using Application.Command.Update;
using Application.Queries.GetDetail;
using Application.Queries.GetList;
using Application.Queries.GetListFiltered;
using DTO;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IMediator _mediator;
        public CategoryController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetListCategories(string langCode = "vi-vn")
        {
            try
            {
                var response = await this._mediator.Send(new GetCategoriesListQuery());
                response = from r in response
                           from q in r.CategoryLanguages
                           where q.LangCode.Equals(langCode)
                           select r;

                var result = new List<CategoryFilterDTO>();
                foreach(var item in response)
                {
                    var cateLanguage = item.CategoryLanguages.FirstOrDefault();
                    var category = new CategoryFilterDTO()
                    {
                        Id = item.Id,
                        Description = cateLanguage.Description,
                        LangCode = cateLanguage.LangCode,
                        Name = cateLanguage.Name
                    };
                    result.Add(category);
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetailCategory(Guid id)
        {
            try
            {
                var getCategoryQuery = new GetCategoryDetailQuery()
                {
                    Id = id
                };

                var response = await this._mediator.Send(getCategoryQuery);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategory(CreateCategoryCommand categoryCommand)
        {
            try
            {
                var response = await this._mediator.Send(categoryCommand);
                if (response)
                {
                    return Ok(201);
                }

                return BadRequest("Can't not create category");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateCategory(UpdateCategoryCommand categoryCommand)
        {
            try
            {
                var response = await this._mediator.Send(categoryCommand);
                if(response)
                {
                    return Ok(204);
                }

                return BadRequest("Can't not update category");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCategory(string id)
        {
            try
            {
                var categoryCommand = new DeleteCategoryCommand { Id = id };
                var response = await this._mediator.Send(categoryCommand);
                if (response)
                {
                    return Ok(204);
                }

                return BadRequest("Can't not delete category");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
