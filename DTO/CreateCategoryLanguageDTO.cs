﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class CreateCategoryLanguageDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string LangCode { get; set; }
    }
}
