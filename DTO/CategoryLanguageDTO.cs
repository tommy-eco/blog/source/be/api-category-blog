﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class CategoryLanguageDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LangCode { get; set; }
    }
}
