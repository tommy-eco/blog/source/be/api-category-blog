﻿using System;
using System.Collections.Generic;

namespace DTO
{
    public class CategoryDTO
    {
        public Guid Id { get; set; }
        public IEnumerable<CategoryLanguageDTO> CategoryLanguages { get; set; }
    }
}
