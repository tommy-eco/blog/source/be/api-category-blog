﻿using Domain;
using Mapster;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Update
{
    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, bool>
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryLanguageRepository _categoryLanguageRepository;
        public UpdateCategoryCommandHandler(ICategoryRepository categoryRepository, ICategoryLanguageRepository categoryLanguageRepository)
        {
            this._categoryRepository = categoryRepository;
            this._categoryLanguageRepository = categoryLanguageRepository;
        }
        public async Task<bool> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            if(request.CategoryLanguages != null)
            {
                if (request.CategoryLanguages.Any())
                {
                    foreach(var item in request.CategoryLanguages)
                    {
                        if(!string.IsNullOrEmpty(item.Id))
                        {
                            var categoryLanguage = await this._categoryLanguageRepository.GetCategoryLanguageAsync(Guid.Parse(item.Id));
                            if (categoryLanguage != null)
                            {
                                categoryLanguage.Name = item.Name;
                                categoryLanguage.Description = item.Description;
                                await this._categoryLanguageRepository.UpdateAsync(categoryLanguage);
                            }
                        }
                        else
                        {
                            var cateLanguage = item.Adapt<CategoryLanguage>();
                            var category = await this._categoryRepository.GetCategoryAsync(Guid.Parse(request.Id));
                            if(category != null)
                            {
                                cateLanguage.Category = category;
                                cateLanguage.Id = Guid.NewGuid();
                                await this._categoryLanguageRepository.CreateAsync(cateLanguage);
                            }
                        }
                    }
                    return true;
                }
            }
            return false;
        }
    }
}
