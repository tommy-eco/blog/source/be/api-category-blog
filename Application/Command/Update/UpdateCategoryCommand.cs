﻿using Domain;
using DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Command.Update
{
    public class UpdateCategoryCommand : IRequest<bool>
    {
        public string Id { get; set; }
        public IEnumerable<CategoryLanguageDTO> CategoryLanguages { get; set; }
    }
}
