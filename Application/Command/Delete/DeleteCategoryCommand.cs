﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Command.Delete
{
    public class DeleteCategoryCommand : IRequest<bool>
    {
        public string Id { get; set; }
    }
}
