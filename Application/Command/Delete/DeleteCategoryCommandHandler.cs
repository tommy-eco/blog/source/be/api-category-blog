﻿using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Delete
{
    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand, bool>
    {
        private readonly ICategoryRepository _categoryRepository;
        public DeleteCategoryCommandHandler(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }
        public async Task<bool> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = await this._categoryRepository.GetCategoryAsync(Guid.Parse(request.Id));
            if(category != null)
            {
                var response = await this._categoryRepository.DeleteAsync(category);
                if(response > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
