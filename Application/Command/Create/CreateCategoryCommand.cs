﻿using DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Command.Create
{
    public class CreateCategoryCommand : IRequest<bool>
    {
        public IEnumerable<CreateCategoryLanguageDTO> CategoryLanguages { get; set; }
    }
}
