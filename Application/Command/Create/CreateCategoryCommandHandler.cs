﻿using Domain;
using Mapster;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, bool>
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryLanguageRepository _categoryLanguageRepository;
        public CreateCategoryCommandHandler(ICategoryRepository categoryRepository, ICategoryLanguageRepository categoryLanguageRepository)
        {
            this._categoryRepository = categoryRepository;
            this._categoryLanguageRepository = categoryLanguageRepository;
        }

        public async Task<bool> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
        {
            // create category
            var category = new Category
            {
                Id = Guid.NewGuid()
            };

            var response = await this._categoryRepository.CreateAsync(category);
            // create category language
            if (response > 0)
            {
                foreach(var item in request.CategoryLanguages)
                {
                    var categoryLanguage = item.Adapt<CategoryLanguage>();
                    categoryLanguage.Id = Guid.NewGuid();
                    categoryLanguage.Category = category;
                    var result = await this._categoryLanguageRepository.CreateAsync(categoryLanguage);
                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
