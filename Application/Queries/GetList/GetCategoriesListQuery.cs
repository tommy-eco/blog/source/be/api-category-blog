﻿using Domain;
using DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Queries.GetList
{
    public class GetCategoriesListQuery : IRequest<IEnumerable<CategoryDTO>>
    {
    }
}
