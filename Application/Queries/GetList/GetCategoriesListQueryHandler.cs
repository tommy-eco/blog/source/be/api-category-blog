﻿using Domain;
using DTO;
using Mapster;
using MediatR;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.GetList
{
    public class GetCategoriesListQueryHandler : IRequestHandler<GetCategoriesListQuery, IEnumerable<CategoryDTO>>
    {
        private readonly IElasticClient _esClient;
        public GetCategoriesListQueryHandler(IElasticClient esClient)
        {
            this._esClient = esClient;
        }
        public async Task<IEnumerable<CategoryDTO>> Handle(GetCategoriesListQuery request, CancellationToken cancellationToken)
        {
            var response = await this._esClient.SearchAsync<CategoryDTO>(
                               s =>
                                    s.From(0)
                                      .Size(10)
                                      .Query(
                                        q => q.MatchAll())
                                    
                                    
                                    );
            if (response.ApiCall.Success)
            {
                return response.Documents.Adapt<IEnumerable<CategoryDTO>>();
            }

            return Enumerable.Empty<CategoryDTO>();
        }
    }
}
