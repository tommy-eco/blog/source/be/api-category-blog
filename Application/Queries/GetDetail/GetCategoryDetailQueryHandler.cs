﻿using Domain;
using DTO;
using MediatR;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Queries.GetDetail
{
    public class GetCategoryDetailQueryHandler : IRequestHandler<GetCategoryDetailQuery, CategoryDTO>
    {
        private readonly IElasticClient _esClient;
        public GetCategoryDetailQueryHandler(IElasticClient esClient)
        {
            this._esClient = esClient;
        }
        public async Task<CategoryDTO> Handle(GetCategoryDetailQuery request, CancellationToken cancellationToken)
        {
            var response = await this._esClient.GetAsync<CategoryDTO>(request.Id);
            if(response.ApiCall.Success)
            {
                return response.Source;
            }
            return new CategoryDTO();
        }
    }
}
