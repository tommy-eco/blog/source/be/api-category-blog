﻿using Domain;
using DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Queries.GetDetail
{
    public class GetCategoryDetailQuery :  IRequest<CategoryDTO>
    {
        public Guid Id { get; set; }
    }
}
