﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface ICategoryLanguageRepository
    {
        Task<IEnumerable<CategoryLanguage>> GetCategoriesLanguageAsync();
        Task<CategoryLanguage> GetCategoryLanguageAsync(Guid id);
        Task<int> CreateAsync(CategoryLanguage categoryLanguage);
        Task<int> DeleteAsync(CategoryLanguage categoryLanguage);
        Task<int> UpdateAsync(CategoryLanguage categoryLanguage);
    }
}
