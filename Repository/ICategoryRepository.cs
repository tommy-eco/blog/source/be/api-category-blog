﻿using Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> GetCategoriesAsync();
        Task<Category> GetCategoryAsync(Guid id);
        Task<int> CreateAsync(Category category);
        Task<int> DeleteAsync(Category category);
        Task<int> UpdateAsync(Category category);
    }
}
