﻿using Domain;
using Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public CategoryRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<int> CreateAsync(Category category)
        {
            await this._dbContext.Categories.AddAsync(category);
            var response  = await this._dbContext.SaveChangesAsync();
            return response;
        }

        public async Task<int> DeleteAsync(Category category)
        {
            this._dbContext.Categories.Remove(category);
            var response = await this._dbContext.SaveChangesAsync();
            return response;
        }

        public async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            var listCategories = await this._dbContext.Categories.ToListAsync();
            return listCategories;
        }

        public async Task<Category> GetCategoryAsync(Guid id)
        {
            var category = await this._dbContext.Categories.FindAsync(id);
            return category;
        }

        public async Task<int> UpdateAsync(Category category)
        {
            this._dbContext.Categories.Update(category);
            var response = await this._dbContext.SaveChangesAsync();
            return response;
        }
    }
}
