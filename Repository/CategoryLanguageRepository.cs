﻿using Domain;
using Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CategoryLanguageRepository : ICategoryLanguageRepository
    {
        private readonly ApplicationDbContext _dbContext; 
        public CategoryLanguageRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public async Task<int> CreateAsync(CategoryLanguage categoryLanguage)
        {
            await this._dbContext.CategoryLanguages.AddAsync(categoryLanguage);
            var response = await this._dbContext.SaveChangesAsync();
            return response;
        }

        public async Task<int> DeleteAsync(CategoryLanguage categoryLanguage)
        {
            this._dbContext.CategoryLanguages.Remove(categoryLanguage);
            var response = await this._dbContext.SaveChangesAsync();
            return response;
        }

        public async Task<IEnumerable<CategoryLanguage>> GetCategoriesLanguageAsync()
        {
            var listCategories = await this._dbContext.CategoryLanguages.ToListAsync();
            return listCategories;
        }

        public async Task<CategoryLanguage> GetCategoryLanguageAsync(Guid id)
        {
            var category = await this._dbContext.CategoryLanguages.FindAsync(id);
            return category;
        }

        public async Task<int> UpdateAsync(CategoryLanguage categoryLanguage)
        {
            this._dbContext.CategoryLanguages.Update(categoryLanguage);
            var response = await this._dbContext.SaveChangesAsync();
            return response;
        }
    }
}
