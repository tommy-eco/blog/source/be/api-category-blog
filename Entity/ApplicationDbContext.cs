﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace Entity
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryLanguage> CategoryLanguages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(c => c.CategoryLanguages)
                .WithOne(c => c.Category)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
